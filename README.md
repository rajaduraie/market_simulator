# market_simulator

This is a mini project to read a live feed from the binary.com website, and then publish the feed. 

A client, which takes in an argument of group id, subscribes to the feed. Based on the group id, specific groups
are filtered and given as output. 

This can be used to build scripts with logic based on prices of the stocks/symbols.
