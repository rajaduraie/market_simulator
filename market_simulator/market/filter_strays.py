#!/usr/bin/env python3

import os
import websocket
import json
import zmq
import argparse
import pprint

from Base import Base

class filter_strays(Base):

  def __init__(self):
    parser = argparse.ArgumentParser()
    parser.add_argument("--tickergroup", help = "Ticker group to filter",
                         default = 'all')
    parser.add_argument("--conf", help = "Configuration file", 
                         default = os.environ['conf'])
    args = parser.parse_args()
    super(filter_strays,self).__init__(args.conf)
    print (self.log)
    self.tickergroup = args.tickergroup
    context = zmq.Context()
    sock = context.socket(zmq.PUB)
    sock.bind("tcp://*:5555")
    self.sock = sock
    
  def start_run(self):
    log = self.log
    log.debug(self.ws.api)
    ws = websocket.WebSocketApp(self.ws.api,
                                on_message = self._on_message, 
                                on_open = self._on_open,
      				on_error = self._on_error)
    log.debug('this will run with the api %s ' % self.ws.api)
    ws.run_forever()

  def _on_open(self, ws):
    self.symbol_data = self._gather_symbols()
    symbols = list()
    for s in self.symbol_data.keys():
      symbols.append(s)
    json_data = json.dumps({'ticks':symbols})
    ws.send(json_data)

  def _on_error(self, ws, error):
    print(error)
    log = self.log
    log.fatal(error)
    self.log.fatal("FATAL ERROR")

  def _gather_symbols(self):
    ticker = {}
    cur = self.db.cursor()
    sql = "SELECT ticker_symbol, ticker_group_id from TICKER "
    if self.tickergroup != 'all':
      sql += "where ticker_group_id = " + self.tickergroup
    cur.execute(sql)
    results = cur.fetchall()
    for res in results:
      ticker[res[0]] = {'symbol': res[0], 'group_id': res[1]}
    return ticker

  def _on_message(self,ws,message):
    data = json.loads(message)
    symbol = data['tick']['symbol']
    tick_detail = json.dumps(data['tick'])
    msg = "{1} {0} {2}".format(data['tick']['symbol'], 
                               self.symbol_data[symbol]['group_id'], 
                               tick_detail)
    self.sock.send_string(msg)
    pprint.pprint('ticks update: %s' % message)

def main():
  self = filter_strays()
  self.start_run()

if __name__ == "__main__":
  main()
