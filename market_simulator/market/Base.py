#/usr/bin/env python3

import sys
import os
import argparse
from lib.base import basefeed,basedb,baselog,basews

class Base():

  def __init__(self, conf=None):
    if conf == None:
      parser = argparse.ArgumentParser()
      parser.add_argument("--conf", help = "Configuration file ",
                           default = os.environ['conf'])
      args = parser.parse_args()
      conf = args.conf

    self.feed = basefeed.basefeed(conf=conf)
    self.db   = basedb.basedb(conf=conf).cnx
    self.log  = baselog.baselog(conf=conf).log
    self.ws   = basews.basews(conf=conf)

def main():
  self = Base()

if __name__ == '__main__':
  main()
