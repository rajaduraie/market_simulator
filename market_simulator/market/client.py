#!/usr/bin/env python3

import websocket
import argparse
import zmq
import json
import re

from Base import Base

parser = argparse.ArgumentParser()
parser.add_argument("--group_id", help = "Group ID ",
                    default = '1')
args = parser.parse_args()
group_id = args.group_id

base = Base()
feed = base.feed
log  = base.log
db   = base.db


# Prepare cursor to insert into alerts and
# stray tables

cur = db.cursor()

ticker_data = {}
average     = {}

context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.setsockopt_string(zmq.SUBSCRIBE, group_id)
socket.connect("tcp://localhost:5555")

def make_ticker_dict( ask = 0.0, bid = 0.0):
  return {'ask' : float(ask), 'bid' : float(bid)}

def get_average(avg = None, ticker = []):
  s = [float(i['bid']) for i in ticker]
  while len(ticker) > feed.average_elements:
    ticker.pop(0)
  avg = sum(s)/len(s)
  return avg

while(True):
  message = socket.recv().decode("utf-8")
  m = re.match('(\d+) (\S+) (.*)$',message)
  grp, sym, ticker_dict = m.group(1), m.group(2), json.loads(m.group(3))
  if sym not in ticker_data.keys():
    ticker_data[sym] = []
  ticker_data[sym].append(make_ticker_dict(ask = ticker_dict['ask'], bid = ticker_dict['bid']))
  average[sym] = get_average(ticker = ticker_data[sym])

  bid = float(ticker_dict['bid'])

  percentage_deviation = abs(bid - average[sym])/bid * 100

  if(percentage_deviation > feed.allowed_deviation):
    print("{0} deviated more than {1} percentage. Could be a stray tick".format(sym, feed.allowed_deviation))
    log.info("Making new entry for " + sym + " in strays as it deviated " + str(percentage_deviation))
    cur.execute("INSERT INTO strays(ticker_symbol, average_value, percentage_deviation) values(%s, %s, %s)", [sym, average[sym], percentage_deviation])
    db.commit()

  if len(ticker_data[sym]) >= feed.average_elements :
    threshold_deviation = abs(bid - ticker_data[sym][0]['bid'])/bid * 100
    if threshold_deviation > feed.threshold_percent :
      log.info("Making new entry for " + sym + " in alerts as it deviated " + str(threshold_deviation))
      print("{0} has changed more than {1}. Check if it needs to be bought or sold".format(sym,feed.threshold_percent))
      cur.execute("INSERT INTO alerts(ticker_symbol, average_value, percentage_deviation) values(%s, %s, %s)", [sym, average[sym], threshold_deviation])
      db.commit()
