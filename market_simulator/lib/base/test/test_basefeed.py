#!/usr/bin/env python3
import unittest
import os
import configparser

from lib.base import basefeed,baselog,basedb,basews

def test_basefeed():
  conf = parse_config_file('feed')
  feed = basefeed.basefeed()
  assert feed.average_elements == conf['averageelementcount']
  assert feed.allowed_deviation == conf['allowedpercentagedeviation']
  assert feed.threshold_percent == conf['thresholdpercentage']

def test_baselog():
  log = baselog.baselog().log
  assert log.name == 'market_feed'
  assert log.getEffectiveLevel() == 10

def test_basedb():
  conf = parse_config_file('database')
  db = basedb.basedb()
  assert db.cnx.get_dsn_parameters()['dbname'] == conf['database']
  assert db.cnx.get_dsn_parameters()['user'] == conf['user']

def test_basews():
  conf = parse_config_file('websocket')
  ws = basews.basews()
  assert ws.api == conf['api']
  
  print(ws)

def parse_config_file(item=None):
  config = configparser.ConfigParser()
  config.read(os.environ['conf'])
  try:
    a = dict(config.items(item))
    return a
  except Exception as e:
    print ("{} section not defined in conf file".format(item))

def main():
  test_basefeed()
  test_baselog()
  test_basedb()
  test_basews()

if __name__ == '__main__':
  main()
