#!/usr/bin/env python3
import sys
import os
import configparser
import argparse

class basefeed():

  def __init__(self, conf=None):
    if conf is None:
      parser = argparse.ArgumentParser()
      parser.add_argument("--conf", help = "Configuration file for websocket details",
                         default = os.environ['conf'])
      args = parser.parse_args()
      self.conf = args.conf
    else:
      self.conf = conf

    feed_conf = self._parse_config_file()
    if bool(feed_conf):
      self.average_elements = int(feed_conf['averageElementCount'])
      self.threshold_percent = float(feed_conf['thresholdPercentage'])
      self.allowed_deviation = float(feed_conf['allowedPercentageDeviation'])
    else:
      print ("Error while retrieving feed parameters")
      sys.exit(1)

  def _parse_config_file(self):
    config = configparser.ConfigParser()
    config.optionxform = str
    config.read(self.conf)
    try:
      a = dict(config.items('feed'))
      return a
    except Exception as e:
      print ("Feed section not defined in conf file")


def main():
  self = basefeed()

if __name__ == '__main__':
  main()

