#!/usr/bin/env python3

import os
import sys
import websocket
import argparse
import configparser

class basews(object):
  
  def __init__(self, conf=None):
    if conf is None:
      parser = argparse.ArgumentParser()
      parser.add_argument("--conf", help = "Configuration file for websocket details",
                         default = os.environ['conf'])
      args = parser.parse_args()
      self.conf = args.conf
    else:
      self.conf = conf

    ws_conf = self._parse_config_file()
    if bool(ws_conf):
      self.api = ws_conf['api']
    else:
      print ("Error while retrieving websocket parameters")
      sys.exit(1)



  def _parse_config_file(self):
    config = configparser.ConfigParser()
    config.read(self.conf)
    try:
      a = dict(config.items('websocket'))
      return a
    except Exception as e:
      print ("WebSocket section not defined in conf file")


def main():
  self = basews()

if __name__ == '__main__':
  main()
