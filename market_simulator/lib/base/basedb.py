#!/usr/bin/env python3

import os
import sys
import psycopg2
import configparser
import argparse

class basedb(object):
  def __init__(self, conf=None):
    if conf is None:
      parser = argparse.ArgumentParser()
      parser.add_argument("--conf", help = "Configuration file for database",
                         default = os.environ['conf'])
      args = parser.parse_args()
      self.conf = args.conf
    else:
      self.conf = conf

    db_conf = self._parse_config_file()
    if bool(db_conf):
      try:
        cnx = psycopg2.connect(**db_conf)
      except:
        print("Unexpected error", sys.exc_info()[0])
        sys.exit(1)
      self.cnx = cnx

  def _parse_config_file(self):
    config = configparser.ConfigParser()
    config.read(self.conf)
    try:
      a = dict(config.items('database'))
      return a
    except Exception as e:
      print ("Database section not defined in conf file")

  def cnx(self):
    return self.cnx

def main():
  self = basedb()

if __name__ == '__main__':
  main()
