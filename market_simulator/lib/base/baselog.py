#!/usr/bin/env python3

import os
import sys
import logging
import argparse
import configparser

class baselog(object):
  def __init__(self, conf=None):
    if conf is None:
      parser = argparse.ArgumentParser()
      parser.add_argument("--conf", help = "Configuration file for logging",
                         default = os.environ['conf'])
      args = parser.parse_args()
      self.conf = args.conf
    else:
      self.conf = conf

    log_conf = self._parse_config_file()
    if bool(log_conf):
      logging.basicConfig(filename=log_conf['file_name'], 
                          level=log_conf['loglevel'])
      self.log = logging.getLogger('market_feed')
    else:
      print ("Error while retrieving logging parameters")
      sys.exit(1)

  def _parse_config_file(self):
    config = configparser.ConfigParser()
    config.read(self.conf)
    try:
      a = dict(config.items('logging'))
      return a
    except Exception as e:
      print ("Logging section not defined in conf file")


def main():
  self = baselog()

if __name__ == '__main__':
  main()


