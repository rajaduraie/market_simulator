
CREATE TABLE CUSTOMER( customer_id  serial PRIMARY KEY, 
                       customer_name VARCHAR(62) NOT NULL,
                       customer_address VARCHAR(128),
                       customer_type VARCHAR(10) NOT NULL
                    );


CREATE TABLE TICKER_GROUP(ticker_group_id serial primary key,
                          ticker_group_name VARCHAR(32) NOT NULL
                         );

CREATE TABLE TICKER (ticker_symbol varchar(32) primary key,
                     ticker_description VARCHAR(128) NOT NULL,
                     ticker_group_id integer references ticker_group(ticker_group_id)
                    );


CREATE TABLE CUSTOMER_PORTFOLIO(customer_id integer references customer(customer_id),
                                ticker_symbol varchar(32) references ticker(ticker_symbol),
                                lower_trigger float,
                                higher_trigger FLOAT,
                                order_status VARCHAR(8) NOT NULL
                               );
                                

CREATE TABLE ORDER_BOOK ( customer_id VARCHAR(32) NOT NULL,
                          ticker_symbol VARCHAR(32) NOT NULL,
                          order_type VARCHAR(6) NOT NULL,
                          number_of_units INT NOT NULL,
                          action VARCHAR(4) NOT NULL,
                          unit_price FLOAT NOT NULL,
                          time_of_action TIMESTAMPTZ NOT NULL
                        );

