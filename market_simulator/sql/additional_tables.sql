create table alerts(alert_id serial primary key,
                    ticker_symbol varchar(32) references ticker(ticker_symbol),
                    average_value numeric(24,4) not null,
                    percentage_deviation numeric(24,4) not null,
                    alert_time timestamp DEFAULT current_timestamp
                   );

alter sequence alerts_alert_id_seq restart with 100;

create table strays(stray_id serial primary key,
                    ticker_symbol varchar(32) references ticker(ticker_symbol),
                    average_value numeric(24,4) not null,
                    percentage_deviation numeric(24,4) not null,
                    stray_time timestamp DEFAULT current_timestamp
                   );

alter sequence strays_stray_id_seq restart with 100;
